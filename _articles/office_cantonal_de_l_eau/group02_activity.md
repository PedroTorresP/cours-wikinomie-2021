---
layout: activity
key: office_cantonal_de_l_eau
title: SPAGE
tags: [information, participation, service public, environnement]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---

### Situation actuelle
Le service de la planification de l’eau possède un outil qui permet de gérer la planification politique publique de l’eau. Il s’agit du schéma de protection, d'aménagement et de gestion des eaux (SPAGE). Cet outil concerne les grands bassins versants hydrologiques du canton.

Il existe 6 SPAGE pour tout le canton qui sont les différents bassins versants hydrologiques:
Aire - Drize
Lac rive gauche
Lac rive droite
Lac - Rhône - Arve
Allondon - Mandement
Champagne - La Laire

#### Règles de gouvernance
Ces données sont publiques mais l’usage est interne au canton. Le SPAGE est présent dans la loi cantonale sur les eaux (L 2 05, art. 13) et son règlement d'exécution (L 2 05.01, art. 7).

#### Responsables et autres acteurs
Les différentes acteurs sont :
Les services de l’Etat
Les communes
Les associations
Le Conseil d’Etat
Le Conseil du développement durable
Les partenaires

Le responsable est l’Office cantonal de l’eau (OCEau). Cependant, le Conseil d’Etat doit approuver chaque SPAGE qui lui-même a été préavisé par le Conseil du développement durable.

#### Résultats et utilisation
Actuellement, seuls 5 SPAGE sont élaborés et adaptés par le Conseil d’Etat. Le SPAGE “Champagne - La Laire” est en cours d’élaboration.

### Analyse des données
#### Source
Les données sont récoltées grâce à la collaboration des acteurs cités précédemment. L’OCEau, les communes ainsi que les partenaires établissent les objectifs à atteindre avec des schémas. Cela prend un an pour élaborer un SPAGE. Ensuite, tous les 6 ans, il est revu et mis à jour. On vérifie si les objectifs ont été atteints ou non.

#### Type
Le SPAGE qui possèdent plusieurs informations:
La description du bassin
L'état des cours d’eau
Qualité de l’eau 
Régime hydrologique
Morphologie des cours d’eau et ouvrages
Espace nécessaire aux cours d’eau et Espace minimal
Valeurs naturelles et paysagères
L’entretien des cours d’eau
Les loisirs
Les eaux souterraines

#### Raison
Le SPAGE permet de coordonner les actions relatives à la gestion des bassins versants hydrographiques du canton. Il sert à planifier et à élaborer des actions. Le but du SPAGE est d’atteindre divers objectifs stratégiques. Cet outil prend la forme d’un rapport qui possède des cartographies.

#### Règles et dispositon
Une fois le SPAGE établie, il est publiquement disponible sur le site de l’Etat de Genève. On retrouve la liste des différents SPAGE sur ce lien : 
https://www.ge.ch/outils-planification-eau/schema-protection-amenagement-gestion-eaux-spage.

La plupart des données récoltées sont aussi mises en ligne sur le SITG. Elles sont accessibles depuis ce lien : 
https://www.etat.ge.ch/geoportail/pro/?mapresources=ASSAINISSEMENT%2CHYDROBIOLOGIE%2CHYDROGRAPHIE&hidden=ASSAINISSEMENT%2CHYDROBIOLOGIE

### Recommandations


### Bibliographie
Office cantonal de l’eau (OCEau). [en ligne]. [Consulté le 11 mai 2021]. Disponible à l’adresse : https://www.ge.ch/organisation/office-cantonal-eau-oceau

Service de la planification de l'eau. [en ligne]. [Consulté le 11 mai 2021]. Disponible à l’adresse : https://www.ge.ch/organisation/oceau-service-planification-eau

Outils de planification de l'eau. [en ligne]. [Consulté le 11 mai 2021]. Disponible à l’adresse : https://www.ge.ch/outils-planification-eau

Schéma de protection, d'aménagement et de gestion des eaux (SPAGE). [en ligne]. [Consulté le 11 mai 2021]. Disponible à l’adresse : https://www.ge.ch/outils-planification-eau/schema-protection-amenagement-gestion-eaux-spage

SPAGE Outil cantonal de gestion intégrée des eaux par bassin versant. [fichier PDF]. [Consulté le 11 mai 2021]. Disponible à l’adresse : https://www.ge.ch/document/eau-spage-outil-cantonal-gestion-integree-eaux-bassin-versant
