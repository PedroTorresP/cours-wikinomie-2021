---
layout: activity
key: office_de_l_urbanisme
title: Participer à un projet urbain
tags: []
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---


### Situation actuelle
L'activité participer à un projet urbain a été mis en place par l'Etat de Genève dans le but de favoriser la concertation de tous les acteurs intéressés par l'aménagement du territoire et les différents projets en cours. 

Actuellement plusieurs démarches sont proposées :
- séances d'information publique
- visites de quartiers
- ateliers
- questionnaires ou enquêtes
- [participer.ge.ch](https://participer.ge.ch/) (plateforme numérique)

Cette plateforme a été mise en place par le canton de Genève pour promouvoir le dialogue public et la participation citoyenne à l'échelle de la région en utilisant le numérique. Il s’agit d’une plateforme basée sur l'outil free open source "Decidim" qui a été développé par une communauté soutenue par la mairie de Barcelone. Cet outil numérique permet donc de poursuivre les démarches de concertation à distance. Ce dispositif n’a pas été conçu pour répondre à l’urgence sanitaire liée au COVID-19. L'objectif était de concevoir un format pour élargir la portée des démarches en présentiel en proposant des modèles alternatifs afin de garantir une participation collective aussi vaste que possible.

Malheureusement le site n'a pas reçu le résultat escompté. Pour l’instant il y a très peu de projets et de participations. L'une des raisons est que la plateforme est méconnue de la population, même de ceux qui s’intéressent à l’urbanisme et à l’évolution du territoire. D'une autre part plusieurs projets qu'ils soient concertés ou non sur le site [participer.ge.ch](participer.ge.ch) font l'objet de référendums. Le problème peut provenir, entre autres, d’un problème de communication et d’information. De plus il est difficile pour le citoyen de se projeter sur des plans architecturaux ou des maquettes à l’échelle. Et donc dans le cadre de notre étude, nous nous sommes principalement intéressés aux projets d'améliorations en cours qui permettraient d'améliorer la participation citoyenne.



#### Règles de gouvernance 


#### Responsables et autres acteurs
- SALMON Christophe – Directeur de la direction administrative et financière (Office de l'urbanisme) 

    


#### Résultats et utilisation



### Analyse des données

#### Source
(Services et services web, autres organisations, comment les données sont collectées)


#### Type
(Informations personelles, données sur un sujet, objet)

#### Raison
(Communication, analyse, décision)

#### Règles et dispositon
(Qui les utlise à l'interieur et l'exterieur, à quelle frequence, qui d'autre a accès, organisation du stockage, quelle licence, données ouvertes?)


### Recommandations


### Bibliographie
(NOM, Prénom, année. Titre de la page web. Nom du site web [en ligne]. Date de publication. Date de la mise à jour. [Consulté le jour mois année]. Disponible à l’adresse : URL)

Participer à un projet urbain. ge.ch [en ligne]. Mise à jour le 7 décembre 2020. [Consulté le 24 mai 2021]. Disponible à l’adresse : [https://www.ge.ch/participer-projet-urbain](https://www.ge.ch/participer-projet-urbain)


La plateforme publique de participation citoyenne pour Genève et sa région. participer [en ligne]. [Consulté le 24 mai 2021]. Disponible à l’adresse : [https://participer.ge.ch/](https://participer.ge.ch/)

Plans localisés de quartier. ge.ch [en ligne]. Mise à jour le 7 décembre 2020. [Consulté le 26 mai 2021]. Disponible à l’adresse : [https://www.ge.ch/consulter-plans-amenagement-adoptes/plans-localises-quartier](https://www.ge.ch/consulter-plans-amenagement-adoptes/plans-localises-quartier) 

Yony, Santos, 2020. Les dé­marches par­ti­ci­pa­tives aux temps de la dis­tan­cia­tion so­ciale. Es­pa­zium [en ligne]. 17.06.2020. [Consulté le 6 juin 2021]. Disponible à l’adresse : [https://www.espazium.ch/fr/actualites/les-demarches-participatives-aux-temps-de-la-distanciation-sociale](https://www.espazium.ch/fr/actualites/les-demarches-participatives-aux-temps-de-la-distanciation-sociale)


