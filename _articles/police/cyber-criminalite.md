---
layout: activity
key: police
title: Cybercriminalité
tags: [participation, cybercriminalité, cybersécurité, service d'intérêt public]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---

### Situation actuelle
La stratégie de lutte contre la cybercriminalité se déroule dans un contexte national, le service de la cybercriminalité de la Police Genevoise est un élément clé de cette dernière. La deuxième édition de cette stratégie a débuté en 2018 et se terminera en 2022. Cette dernière est composée de plusieurs secteurs. Il y a le service de cybercriminalité de l’armée qui se nomme “cyber-commande”, le service de protection d’infrastructure critique ou vitale, la protection des infrastructures informatique du gouvernement. Le dernier secteur est celui qui nous intéressera le plus. Il s’agit de la poursuite pénale, c’est dans ce secteur que se trouve le service de la cybercriminalité de la Police Genevoise. Dans ce secteur, il existe plusieurs commissions, celui qui nous intéresse le plus se nomme “cybercase”. Le “cybercase” est séparé par différents axes, il y a le SPOC (Single Point of Contact) le NEDIC et le RC3 (Regional Cyber Competence Center).

Le SPOC est composé d'un service par canton. Le SPOC du canton de Genève est géré par Mme. Sethi-Karam.

Le NEDIC est un réseau de soutien aux enquêteurs IT au sein de la police. Le but de cette section est de permettre d’avoir une vue d’ensemble des affaires nationales pour tout ce qui est de matière cyber notamment en harmonisant les réponses par thèmes par exemple. Elle se divise en 3 pôles qui sont : la lutte contre les ransomware, la lutte contre la pédopornographie sur Internet, et PIXEL.

Le centre national des ransomwares se trouve à Genève et il assure toute la coordination de lutte contre les ransomwares en Suisse.

La lutte contre la pédopornographie était gérée par FEDPOL jusqu’au 31 décembre 2020. Leurs rôles étaient principalement d’effectuer des analyses de tout ce qui concerne les transferts de fichier du réseau peer-to-peer notamment. Le 1 janvier 2021, Berne était censé récupérer la charge de ce secteur pour toute la Suisse. Malheureusement, il se trouve qu’ils n’étaient pas prêts pour récupérer ce travail. Par conséquent, Genève s’est proposé pour récupérer ce secteur, car un service était déjà existant depuis 2017 et a connu une forte accélération de son développement en 2019. La lutte contre la pédopornographie est gérée par la Brigade de Criminalité Informatique de la Police Genevoise. Ce service a été créé et est géré par un instructeur d’Europole. Son  rôle est de former les policiers européens dans la thématique de la lutte contre la pédopornographie. Grâce à ce service, Genève a pu devenir le centre de compétence romand dans un premier temps. Ce service a deux missions principales qui sont : 

- Le monitoring peer-to-peer pour détecter les échanges de fichier illicites.
- Aller sur des chats et forum pour détecter et identifier d'éventuels prédateurs sexuels qui pourraient aller jusqu’à la rencontre dans la vraie vie. Des policiers sont formés pour rester crédibles et éventuellement aller jusqu’à la rencontre. Ils travaillent dans le cadre de la recherche secrète donc il s’agit d’agent sous couverture sur Internet et ils ne doivent à aucun moment créer le délit. En effet, ils doivent faire en sorte de ne pas encourager la personne à aller jusqu’au contact.

La Plateforme intercantonale sur les délits sériels en ligne (PIXEL) est une collecte de données au niveau des plaintes. En effet, il s’agit d’une base de données centralisée, située à Neuchâtel, dont le but est de centraliser les informations afin de répondre à une problématique. La problématique est la suivante : deux personnes sont victimes d'escroquerie sur Internet et vont déposer plainte à la police. Ces deux personnes sont victimes du même auteur, mais aucun outil ne permet de le savoir. Par conséquent, les plaintes vont arriver dans une brigade chez deux enquêteurs différents qui vont adresser une demande à la brigade de criminalité informatique où deux spécialistes cyber vont travailler sur les mêmes plaintes sans savoir qu'ils travaillent sur le même auteur. Ensuite, cela remontera jusqu'au ministère public et éventuellement, elles peuvent arriver chez deux procureurs différents. L’idée de PIXEL est donc d'enregistrer dès la prise de plainte des informations techniques dans la base de données (IP, pseudo, email, heure, type d’attaque, ...) où une analyse se fait ensuite par la brigade de renseignement criminel dans les 24 heures qui suive dans le but d’établir des séries. En revanche, PIXEL est encore en cours de construction, pas tous les cantons l’utilisent encore. Seuls les cantons faisant partie d’un concordat l’utilisent actuellement. L’objectif initial pour Genève est de se coordonner au niveau romand et par la suite au niveau national.

Enfin, en ce qui concerne le centre de compétence (RC3), l’idée est de mutualiser toutes les informations qui sont chères ou rares au sein d’un même centre. Pour la Suisse occidentale, le centre se trouve à Genève. Ce centre de compétence comprend les trois brigades de la section forensique. 

Ce centre de compétence couvre 2 axes principaux qui sont : 
- L’analyse de support de données avancé (IOT)
- L’interception, ce qui regroupe notamment l’infection par cheval de Troie des ordinateurs ou des mobiles de personnes suspectes.


#### Règles de gouvernance
Au sein de Cybercase, nous avons la section Forensique qui est située à Genève, elle gère tout ce qui est dans le cadre de la lutte contre la cybercriminalité et le chef de cette section est M.Ghion. Dans cette section, nous avons 16 policiers et 3 ingénieurs qui sont répartis dans 3 brigades différentes qui sont :

- la Brigade de Criminalité Informatique (BCI), 
- la Brigade de Renseignements Criminel (BRC),
- la Brigade des Cyber Enquêtes (BCE, qui sera ouverte le 1er septembre 2021).

De manière générale, les données récoltées par la police sont issues des plaintes déposées. Celles-ci sont directement stockées dans les bases polices et sont accessibles par tous les policiers.

Un cyberkiosque disponible en ligne permet aux citoyens ou à des organisations de poser des questions et de signaler des cyber-incidents à la police.  Le cyberkiosque est géré par la Brigade de Renseignements Criminels (BRC) dont leurs activités principales sont de répondre à toutes les questions et, suivant les cas, d’orienter les victimes vers des dépôts de plainte. Ils reçoivent environ 5 à 15 formulaires par semaine. Les informations récoltées par ce service sont les données du formulaire en ligne décrivant l’incident. 


#### Responsables et autres acteurs
- Cap. Patrick Ghion (Chef de la section forensique)
- 16 policiers
- 3 ingénieurs


#### Résultats et utilisation
Pour ce qui est de PIXEL, toutes les données techniques sont stockées et centralisées dans le but d’éviter le travail à double et de permettre une réponse pénale plus appropriée. Des séries seront donc mises en place et elles permettront de regrouper les attaques du même type. Par conséquent, ces données seront utilisées pour identifier et classer les différentes escroqueries d’Internet.


### Analyse des données

#### Source
Lorsqu’il y a une plainte, les données sont enregistrées dans le système police et accessibles par tout le corps de police. Ensuite, un transfert des données techniques est fait sur la plateforme PIXEL. Ce transfert se fait automatiquement, semi-automatiquement ou manuellement selon les cantons.

#### Type
Pour ce qui est du système de police, les données sont principalement de type texte, date et des chiffres.

Pour ce qui est de PIXEL, les données stockées concernent uniquement la cybercriminalité. Celles-ci sont du même ordre que pour le système police, il s’agit de texte, date et chiffres. 

#### Raison
Le but de la plateforme PIXEL est d’éviter le travail à double et de permettre une réponse pénale plus appropriée. Jusqu’à présent il y avait plusieurs victimes du même auteur qui était jugé séparément. En effet, chaque escroquerie avait une valeur différente alors qu’ils provenaient de la même organisation. Grâce à PIXEL, ils pourront avoir une valeur totale pour une même escroquerie. Par exemple, ils n’auront plus 300 plaintes identiques valant 200 francs chacune mais 300 plaintes valant 60’000 CHF au total.

En ce qui concerne le formulaire en ligne, celui-ci sert principalement à rassurer la population et à informer la police des tendances actuelles en termes de cybercriminalité. En effet, Le cyberkiosque permet à la police d’être au courant des différentes cyberattaques actuelles car mis-à-part les données récoltées issues des plaintes et des relations privilégiées avec des grandes entreprises,  ils ne récoltent aucune autre information. Les résultats récoltés vont également leur permettre d’assurer une réponse de prévention envers la population. En effet, si un phénomène vient à émerger, ils lancent une campagne de prévention. 

#### Règles et disposition
En ce qui concerne PIXEL, les données stockées dans la base de données située à Neuchâtel sont des données techniques issues de plaintes. Il s’agit notamment des adresses IP utilisées, des adresses mail, des pseudos, du type d’attaques, l’heure de survenue de l’attaque, etc. Toutes ses données ne sont pas accessibles au corps de police. En effet, une seule personne par canton a le droit d’accéder à ses données. Il s’agit des responsables PIXEL. Ceci s’explique au fait qu’il n’y a pas de gestion de droit sur PIXEL et par conséquent, le mettre entre les mains de tout le monde représenterait un danger.

Le cyberkiosque est géré par la brigade de renseignements criminels (BRC) dont leurs activités principales sont de répondre à toutes les questions et, suivant les cas, d’orienter les victimes vers des dépôts de plainte. Ils reçoivent environ 5 à 15 formulaires par semaine. Les données récoltées de ces formulaires sont stockées sur la base de données du système police, elles sont disponibles uniquement par le service police, il n’y aucun accès public.

Certaines statistiques ou données récoltées par le service de cybercriminalité sont transmises à CATFISH.


### Recommandations
- Rendre les transferts des données du système police à PIXEL automatiques, Genève semi-automatique
- Renforcer le service cybercriminalité dans les autres cantons
- Ajouter une gestion de droit sur le service PIXEL

### Bibliographie
ÉTAT DE GENÈVE, 2017. Signaler un cyber-incident à la police. GE.CH – République et canton de Genève [en ligne]. 12 juin 2017. Dernière modification le 8 mars 2018 [Consulté le 4 avril 2021]. Disponible à l’adresse : https://www.ge.ch/criminalite-internet/signaler-cyber-incident-police.

Certaines informations proviennent des interviews réalisées avec le Cap. Patrick Ghion, chef de la section Forensique.
