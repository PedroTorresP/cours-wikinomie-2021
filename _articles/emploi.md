---
layout: theme
title: "Emploi"
key: emploi
is_main: true
toc:
- "Contexte"
---

### Contexte

L'emploi est un sujet qui touche à la population entière, tout comme le chômage. Il reste tout de même primordial d'analyser les services proposés en lien avec ce sujet car plus il est important - un plus grand nombre de personne sont touchées - plus il faut être transparent et fournir toutes les informations nécessaires aux citoyens.

L'inscription au chômage est dorénavant possible via un formulaire en ligne, il est donc important d'être transparent avec la population quant à l'utilisation des données qui sont entrées. Les deux activités référencées sont fortement liées l'une à l'autre, c'est en s'inscrivant au chômage que l'on peut avoir accès à la plateforme [job-room.ch](https://www.job-room.ch/home/job-seeker), c'est pour cette raison qu'il a été important d'effectuer des recherches sur ces deux services proposés et analyser leurs transparence, identifier les lacunes et proposés des améliorations afin de satisfaire la doctrine du gouvernement ouvert.

